#include "Application.hpp"
#include "ShaderProgram.hpp"

#include <iostream>
#include <vector>

namespace
{
    const int numParaboloids = 1; //2;
    const int nBeamsPerSide = 1;
}

class SampleApplication : public Application
{
public:
    ShaderProgramPtr _standardShader;
    ShaderProgramPtr _computeShader;

    // начала параболоидов (в мировых координатах)
    std::vector<glm::vec3> _paraboloidCenters;
    // параметры параболоидов (a, b, ограничение по высоте)
    std::vector<glm::vec3> _paraboloidParams;
    // цвета параболоидов
    std::vector<glm::vec4> _paraboloidColors;

    GLuint _paraboloidCentersVbo;
    GLuint _paraboloidParamsVbo;
    GLuint _paraboloidColorsVbo;
    GLuint _paraboloidVao;

    std::vector<glm::vec3> _beamStarts;
    std::vector<glm::vec4> _colors;

    GLuint _beamStartsVbo;
    GLuint _colorsVbo;
    GLuint _nodesVao;

    int nBeams = -1;

    void make_vertices(int width, int height, int N, bool dump = false) {
        nBeams = width * height * N * N;
        _colors.resize( nBeams, glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
        _beamStarts.resize( nBeams );
        // разбитие пиксельной сеткой отрезка [-1,1]
        const float ph = (float)2 / height;
        const float pw = (float)2 / width;
        for (int k = 0; k < width; ++k) {
            // генерируем сеткой внутри каждого пикселя равномерно n^2 точек
            const float phs = -1.0f + pw * k;
            const int id1 = N * N * height * k;
            for (int m = 0; m < height; ++m) {
                const float pvs = -1.0f + ph * m;
                const int id2 = id1 + N * N * m;
                for (int i = 0; i < N; ++i) {
                    const float x = phs + ((float)(i + 1) / (N + 1)) * pw;
                    const int id3 = id2 + N * i;
                    for (int j = 0; j < N; ++j) {
                        const float y = pvs + ((float)(j + 1) / (N + 1)) * ph;
                        const int id = id3 + j;
                        _beamStarts[id] = glm::vec3(x, y, 0.0f);
                        if (dump) {
                            std::cout << _beamStarts[id].x << " " << _beamStarts[id].y << " | ";
                        }
                    }
                }
                if (dump) {
                    std::cout << std::endl;
                }
            }
        }

    }

    void makeScene() override
    {
        Application::makeScene();
        //=========================================================
        _cameraMover = std::make_shared<FreeCameraMover>();

        //Инициализация шейдеров
        _standardShader = std::make_shared<ShaderProgram>("693RiashchikovData1/simple.vert", "693RiashchikovData1/simple.frag");

        _computeShader = std::make_shared<ShaderProgram>();
        ShaderPtr vs = std::make_shared<Shader>(GL_COMPUTE_SHADER);
        vs->createFromFile("693RiashchikovData1/compute_shader.comp");
        _computeShader->attachShader(vs);
        _computeShader->linkProgram();

        //=========================================================

        _paraboloidCenters.resize(numParaboloids, glm::vec3());
        _paraboloidParams.resize(numParaboloids, glm::vec3());
        _paraboloidColors.resize(numParaboloids, glm::vec4());

        _paraboloidCenters[0] = glm::vec3(0.0f, 0.0f, -2.0f);
        _paraboloidParams[0] = glm::vec3(2.0f, 2.0f, 150.0f );
        _paraboloidColors[0] = glm::vec4( 0.0f, 1.0f, 0.0f, 1.0f);

        //_paraboloidCenters[1] = glm::vec3(1000.0f, 500.0f, -2.0f);
        //_paraboloidParams[1] = glm::vec3(10.0f, 8.0f, 900.0f );
        //_paraboloidColors[1] = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f );

        _paraboloidCentersVbo = 0;
        glGenBuffers(1, &_paraboloidCentersVbo);
        glBindBuffer(GL_ARRAY_BUFFER, _paraboloidCentersVbo);
        glBufferData(GL_ARRAY_BUFFER, _paraboloidCenters.size() * sizeof(float) * 3, _paraboloidCenters.data(), GL_STREAM_DRAW);

        _paraboloidParamsVbo = 0;
        glGenBuffers(1, &_paraboloidParamsVbo);
        glBindBuffer(GL_ARRAY_BUFFER, _paraboloidParamsVbo);
        glBufferData(GL_ARRAY_BUFFER, _paraboloidParams.size() * sizeof(float) * 3, _paraboloidParams.data(), GL_STREAM_DRAW);

        _paraboloidColorsVbo = 0;
        glGenBuffers(1, &_paraboloidColorsVbo);
        glBindBuffer(GL_ARRAY_BUFFER, _paraboloidColorsVbo);
        glBufferData(GL_ARRAY_BUFFER, _paraboloidColors.size() * sizeof(float) * 4, _paraboloidColors.data(), GL_STREAM_DRAW);
        //--------------------------------

        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, _paraboloidCentersVbo);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, _paraboloidParamsVbo);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, _paraboloidColorsVbo);

        //--------------------------------

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        std::cout << width << " " << height << std::endl;

        make_vertices(width, height, nBeamsPerSide);

        _beamStartsVbo = 0;
        glGenBuffers(1, &_beamStartsVbo);
        glBindBuffer(GL_ARRAY_BUFFER, _beamStartsVbo);
        glBufferData(GL_ARRAY_BUFFER, _beamStarts.size() * sizeof(float) * 3, _beamStarts.data(), GL_STREAM_DRAW);

        _colorsVbo = 0;
        glGenBuffers(1, &_colorsVbo);
        glBindBuffer(GL_ARRAY_BUFFER, _colorsVbo);
        glBufferData(GL_ARRAY_BUFFER, _colors.size() * sizeof(float) * 4, _colors.data(), GL_STREAM_DRAW);

        _nodesVao = 0;
        glGenVertexArrays(1, &_nodesVao);
        glBindVertexArray(_nodesVao);

        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, _beamStartsVbo);
        glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, NULL);

        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, _colorsVbo);
        glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, NULL);

        glBindVertexArray(0);

        //--------------------------------

        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, _beamStartsVbo);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, _colorsVbo);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
        }
        ImGui::End();
    }

    void update()
    {
        Application::update();
    }

    void draw() override
    {
        // Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);
        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Просчитываем цвета каждого луча
        updateIntersections(_computeShader);
        // Блокируем дальнейшее выполнение, пока не завершится запись в SSBO
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        drawScreen(_standardShader);

        glUseProgram(0);
    }

    void updateIntersections(const ShaderProgramPtr& shader)
    {
        shader->use();

        shader->setMat4Uniform("viewMatrixInv", glm::inverse(_camera.viewMatrix));
        
        //glm::mat4 view = _camera.viewMatrix;
        //glm::mat4 invView = glm::inverse(_camera.viewMatrix);

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glm::mat4 projMat = glm::ortho(-(float)width / 2, (float)width / 2 ,  -(float)height / 2, (float)height / 2, 0.1f, 50.0f);
        
        //sconst float aspect = float(height) / width;
        //glm::mat4 projMat = glm::ortho(-1.0f, 1.0f, -1.0f * aspect, 1.0f * aspect, 0.1f, 100.0f);
        //glm::mat4 invProjMat = glm::inverse(projMat);
        //glm::vec4 center0 = projMat * view * glm::vec4(_paraboloidCenters[0], 1.0f);
        //glm::vec4 center1 = projMat * view * glm::vec4(_paraboloidCenters[1], 1.0f);
        //std::cout << center0.x << " " << center0.y << " " << center0.z << " " << center0.w << std::endl;
        //std::cout << center1.x << " " << center1.y << " " << center1.z << " " << center1.w << std::endl;

        shader->setMat4Uniform("projectionMatrixInv", glm::inverse(projMat));
        
        shader->setIntUniform("numParaboloids", numParaboloids);
        shader->setIntUniform("numBeams", nBeams);
        shader->setFloatUniform("MAX_FLOAT", FLT_MAX);

        unsigned int pCentersIdx = glGetProgramResourceIndex(shader->id(), GL_SHADER_STORAGE_BLOCK, "ParaboloidCenters");
        glShaderStorageBlockBinding(shader->id(), pCentersIdx, 0); // 0я точка привязки

        unsigned int pParamsIdx = glGetProgramResourceIndex(shader->id(), GL_SHADER_STORAGE_BLOCK, "ParaboloidParams");
        glShaderStorageBlockBinding(shader->id(), pParamsIdx, 1); // 1я точка привязки

        unsigned int pColorsIdx = glGetProgramResourceIndex(shader->id(), GL_SHADER_STORAGE_BLOCK, "ParaboloidColors");
        glShaderStorageBlockBinding(shader->id(), pColorsIdx, 2); // 2я точка привязки

        unsigned int bStartsIdx = glGetProgramResourceIndex(shader->id(), GL_SHADER_STORAGE_BLOCK, "BeamStarts");
        glShaderStorageBlockBinding(shader->id(), bStartsIdx, 3); // 3я точка привязки

        unsigned int bColorsIdx = glGetProgramResourceIndex(shader->id(), GL_SHADER_STORAGE_BLOCK, "BeamColors");
        glShaderStorageBlockBinding(shader->id(), bColorsIdx, 4); // 4я точка привязки

        // 128 - количество вызовов внутри одной рабочей группы (оно задается в шейдере)
        int blockSize = 128;
        std::cout << width << " " << height << std::endl;
        glDispatchCompute((nBeams + blockSize - 1) / blockSize, 1, 1);
    }

    void drawScreen(const ShaderProgramPtr& shader)
    {
        shader->use();

        glEnable(GL_PROGRAM_POINT_SIZE);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glDepthMask(false);

        glBindVertexArray(_nodesVao); // Подключаем VertexArray
        glDrawArrays(GL_POINTS, 0, nBeams); //Рисуем

        glDepthMask(true);

        glDisable(GL_BLEND);
        glDisable(GL_PROGRAM_POINT_SIZE);
    }
};

int main()
{
    SampleApplication app;
    app.start();
    return 0;
}